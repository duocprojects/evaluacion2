
""" 
Props:

Rut (sin dígito verificador ni puntos).
Nombre
Dirección
Correo electrónico
Edad
NEM
 """

import time, os

os.system("cls")

user = 'admin'
password = 'admin'

while True: 
    print("\t\tSistema de Gestión de Alumnos")
    print("1. Registrar alumno")
    print("2. Consultar datos de alumno")
    print("3. Salir")
    try: 
        menuOption = int(input("Ingrese opción: "))
        if menuOption == 1:
            os.system("cls")
            print("Registro de alumnos")
            
            #Validación de nombre
            while True:
                nombre = input("Ingrese su nombre: ")
                if len(nombre) <= 0:
                    print("El campo nombre no puede venir vacío.")
                else:
                    break
            
            #Validación de dirección
            while True:
                direccion = input("Ingrese dirección: ")
                if len(direccion) <= 0:
                    print("El campo dirección no puede venir vacío.")
                else:
                    break
                
            #Validación de RUT
            while True:
                try:
                    rut = int(input("Ingrese RUT (Sin dígito verificador y guión): "))
                    if rut <= 500000 or rut >= 39999999:
                        print("El rut debe ser mayor o igual a 5M y menor a 40M")
                    else:
                        break
                except:
                    print("Rut no válido (Ingresa sin dígito verificador y guión)")
            
            #Validación de correo
            while True:
                correo_electronico = input("Ingresa el correo: ")
                if '@' not in correo_electronico:
                    print("Ingresa un correo válido. (example@mail.cl)")
                else:
                    break
            
            #Validación de edad
            while True:
                try:
                    edad = int(input("Ingresa la edad: "))
                    if 17 <= edad <= 90:
                        break
                    else:
                        print("Ingresa una edad entre 17 y 90.")
                except:
                    print("Ingresa un número válido.")
                    
            #Validación de NEM
            while True:
                try:
                    nem = float(input("Ingrese el NEM: "))
                    if 100 <= nem <= 1000:
                        break
                    else:
                        print("Ingresa un NEM válido.")
                except:
                    print("Ingresa un NEM válido.")
                
                
        elif menuOption == 2:
            print("Consulta de datos.")
            usuarioInput = input("Ingrese usuario: ")
            passwordInput = input("Ingrese password: ")
            
            if usuarioInput == user and passwordInput == password:
                rutInput = int(input("Ingrese rut del alumno: "))
                if rutInput == rut:
                    print("Nombre: ", nombre)
                    print("Rut: ", rut)
                    print("Correo: ", correo_electronico)
                    print("Dirección: ", direccion)
                    print("Edad: ", edad)
                    print("NEM: ", nem)
                    if nem >= 520:
                        print('alumno cumple con requisitos' )
                    else:
                        print('alumno no cumple con requisitos')
                else:
                    print("Alumno no encontrado.")
            else:
                print("No tienes permiso para visualizar la info")
        elif menuOption == 3:
            print("Saliendo del programa...")
            time.sleep(5)
            break
    except:
        print("Opción no existe.")